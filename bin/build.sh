#!/usr/bin/env bash

set -e
set -x

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src

mkdir -p ${dist_dir}

rsync -aH $src_dir/ $dist_dir

# libreoffice
libre_src_files=$(find -L ${src_dir} -maxdepth 1 -name '*.fodg' -o -name '*.fodt' -o -name '*.odg' -o -name '*.odt' -type f | sort)

for libre_src_file in ${libre_src_files}; do
  for format in pdf html; do
    libreoffice --invisible --headless --convert-to ${format} ${libre_src_file} --outdir ${dist_dir}
  done
done

# pdf output
pdf_files=$(cd ${dist_dir} && find . -type f -maxdepth 1 -name '*.pdf')

IFS=$'\n'
for pdf_file in $pdf_files
do
  basename=$(basename -- "${pdf_file}")
  dirname=$(dirname -- "${pdf_file}")
  filename="${basename%.*}"

  page1_pdf_dir=${dist_dir}/${dirname}
  page1_pdf="${page1_pdf_dir}/${filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${dist_dir}/${pdf_file}

  page1_pdf_png="${page1_pdf}.png"

  convert ${page1_pdf} ${page1_pdf_png}

  for size in 600 450 350 250 150 100 65 45
  do
    page1_pdf_png_size="${page1_pdf}-${size}.png"
    convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
  done
done
IFS="$OIFS"
